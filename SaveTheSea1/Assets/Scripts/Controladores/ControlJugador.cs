﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            GetComponent<Animator>().SetBool("swim", true);
        } else
        {
            GetComponent<Animator>().SetBool("swim", false);
        }
    }
}

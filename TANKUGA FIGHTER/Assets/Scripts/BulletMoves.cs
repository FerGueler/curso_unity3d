﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMoves : MonoBehaviour
{
    private Vector3 playerDir1 = Vector3.left;
    GameObject theTank;
    //public GameObject player;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        theTank = GameObject.Find("Tank1");
        MoveTank tankScript = theTank.GetComponent<MoveTank>();
        playerDir1 = tankScript.playerDir;


    }

    // Update is called once per frame
    void Update()

    {
        // ahora mismo al disparo del Tank2 se le aplica la rotacion del tank1, si
        // su rotacion es 0 (mira hacia arriba) dispara bien
        // Tank1 dispara con rotacion doble

        transform.Translate( playerDir1 * bulletSpeed * Time.deltaTime);
    }
}

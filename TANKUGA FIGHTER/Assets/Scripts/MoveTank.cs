﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTank : MonoBehaviour
{
    public float  tankSpeed = 30;
    public float rotateSpeed = 5;
    public float xBound = 6;
    public float yBound = 4;
    public GameObject bullet;
    public Rigidbody2D bulletRB;
    Quaternion rotationQuaternion = Quaternion.Euler(0, 0, 0);
    public float bulletSpeed;
    private Transform myTransform;
    public Vector3 playerDir;


    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        playerDir = transform.rotation * Vector3.up;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Vector3 vectorMov = Vector3.up * tankSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            Vector3 vectorMov = Vector3.down * tankSpeed * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Vector3 vectorMov = Vector3.forward * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = Vector3.back * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }
        //shoot

        // make playerDir;

        if (Input.GetKeyDown(KeyCode.RightControl))
        {
            Instantiate(bullet, transform.position, transform.rotation);
            
            //Rigidbody2D bulletInstance = Instantiate(bulletRB, transform.position, Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            //bulletInstance.velocity = new Vector2(bulletSpeed, 0);


        }

        //boundaries

        if (transform.position.x > xBound)
        {
            transform.position = new Vector3(xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.x < -xBound)
        {
            transform.position = new Vector3(-xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.y > yBound)
        {
            transform.position = new Vector3(transform.position.x, yBound, transform.position.z);
        }
        if (transform.position.y < -yBound)
        {
            transform.position = new Vector3(transform.position.x, -yBound, transform.position.z);
        }   
    }
}

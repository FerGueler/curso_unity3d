﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTank2 : MonoBehaviour
{

    public float tankSpeed = 30;
    public float rotateSpeed = 5;
    public float xBound = 6;
    public float yBound = 4;
    public GameObject bullet;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //move
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 vectorMov = Vector3.up * tankSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 vectorMov = Vector3.down * tankSpeed * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
        }
        
        //rotate

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 vectorMov = Vector3.forward * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 vectorMov = Vector3.back * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        //shoot
        if (Input.GetKeyDown(KeyCode.F))
        {
            Instantiate(bullet, transform.position, transform.rotation);


        }


        //boundaries

            if (transform.position.x > xBound )
            {
            transform.position= new Vector3 (xBound, transform.position.y, transform.position.z);
            }
            if (transform.position.x < -xBound)
            {
            transform.position = new Vector3(-xBound, transform.position.y, transform.position.z);
            }
            if (transform.position.y > yBound )
            {
            transform.position = new Vector3(transform.position.x, yBound , transform.position.z);
            }
            if ( transform.position.y < -yBound)
            {
            transform.position = new Vector3(transform.position.x, -yBound, transform.position.z);
            }

    }
}


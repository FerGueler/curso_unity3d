﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoveTank : MonoBehaviour
{
    public float  tankSpeed = 30;
    public float rotateSpeed = 5;
    public float xBound = 6;
    public float yBound = 4;
    public GameObject bullet;
    public Rigidbody2D bulletRB;
    
    public float bulletSpeed;
  
    public Vector3 playerDir;
    public int tank1HP = 3;
    private float tank1Displacement = 0.3f;
    private float fireRate = 1f;
    private float nextFire = 0.0f;
    private float fireRateIncrease = 0.15f;
    public TextMeshProUGUI hp1Text;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //playerDir = transform.rotation * Vector3.up;
        playerDir = transform.up;
        //Debug.Log(playerDir);
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Vector3 vectorMov = Vector3.up * tankSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            Vector3 vectorMov = Vector3.down * tankSpeed * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Vector3 vectorMov = Vector3.forward * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = Vector3.back * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }
        //shoot

        // make playerDir;

        if (Input.GetKeyDown(KeyCode.RightShift) && Time.time > nextFire)
        {
            Instantiate(bullet, transform.position + transform.up * tank1Displacement, bullet.transform.rotation);
            nextFire = Time.time + fireRate;
            fireRate = fireRate + fireRateIncrease;
            //Rigidbody2D bulletInstance = Instantiate(bulletRB, transform.position, Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            //bulletInstance.velocity = new Vector2(bulletSpeed, 0);


        }

        //boundaries

        if (transform.position.x > xBound)
        {
            transform.position = new Vector3(xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.x < -xBound)
        {
            transform.position = new Vector3(-xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.y > yBound)
        {
            transform.position = new Vector3(transform.position.x, yBound, transform.position.z);
        }
        if (transform.position.y < -yBound)
        {
            transform.position = new Vector3(transform.position.x, -yBound, transform.position.z);
        }
       
        hp1Text.SetText("HP1: " + tank1HP);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet2"))
        {
            Destroy(collision.gameObject);
            tank1HP--;
            if (tank1HP < 1)
            {
                hp1Text.SetText("HP1: " + tank1HP);
             Destroy(gameObject);} 

        }
        
    }

}

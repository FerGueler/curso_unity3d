﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoveTank2 : MonoBehaviour
{

    public float tankSpeed = 30;
    public float rotateSpeed = 5;
    public float xBound = 6;
    public float yBound = 4;
    public GameObject bullet;
    public Vector3 player2Dir;
    public int tank2HP = 3;
    private float tank2Displacement = 0.3f;
    private float fireRate = 1f;
    private float nextFire = 0.0f;
    private float fireRateIncrease = 0.15f;
    public TextMeshProUGUI hp2Text;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //player2Dir = transform.rotation * Vector3.up;
        player2Dir = transform.up;
        //move
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 vectorMov = Vector3.up * tankSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 vectorMov = Vector3.down * tankSpeed * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
        }

        //rotate

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 vectorMov = Vector3.forward * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 vectorMov = Vector3.back * rotateSpeed * Time.deltaTime;
            this.GetComponent<Transform>().Rotate(vectorMov);
        }

        //shoot
        if (Input.GetKeyDown(KeyCode.F) && Time.time > nextFire)
        {
            
            Instantiate(bullet, transform.position+transform.up*tank2Displacement, bullet.transform.rotation);
            nextFire = Time.time + fireRate;
            fireRate = fireRate + fireRateIncrease;
        }


        //boundaries

        if (transform.position.x > xBound)
        {
            transform.position = new Vector3(xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.x < -xBound)
        {
            transform.position = new Vector3(-xBound, transform.position.y, transform.position.z);
        }
        if (transform.position.y > yBound)
        {
            transform.position = new Vector3(transform.position.x, yBound, transform.position.z);
        }
        if (transform.position.y < -yBound)
        {
            transform.position = new Vector3(transform.position.x, -yBound, transform.position.z);
        }
        

    }
    private void LateUpdate()
    {
       hp2Text.SetText("HP2: " + tank2HP);
    }
    //gets hit
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet1"))
        {
            
            Destroy(collision.gameObject);
            tank2HP--;
            if (tank2HP == 0)
            { hp2Text.SetText("HP2: " + tank2HP);
                Destroy(gameObject); }


        }
        
        
    }

}

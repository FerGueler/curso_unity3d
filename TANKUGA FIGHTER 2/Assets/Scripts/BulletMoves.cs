﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMoves : MonoBehaviour
{
    private Vector3 playerDir1;
    GameObject theTank;
    //public GameObject player;
    public float bulletSpeed;
    public float bulletLifetime;
    // Start is called before the first frame update
    void Start()
    {
        theTank = GameObject.Find("Tank1");
        MoveTank tankScript = theTank.GetComponent<MoveTank>();
        playerDir1 = tankScript.playerDir;
        // Calling BulletDies funcion
        StartCoroutine(BulletDies());

    }

    // Update is called once per frame
    void Update()

    {
        // ahora mismo al disparo del Tank2 se le aplica la rotacion del tank1, si
        // su rotacion es 0 (mira hacia arriba) dispara bien
        // Tank1 dispara con rotacion doble

        transform.Translate(playerDir1.normalized * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall Vertical"))
        {
            playerDir1 = new Vector3(-playerDir1.x, playerDir1.y, playerDir1.z);


        }
        if (collision.gameObject.CompareTag("Wall Horizontal"))
        {
            playerDir1 = new Vector3(playerDir1.x, -playerDir1.y, playerDir1.z);


        }
    }

    //function to make bullet die after some time
    IEnumerator BulletDies()
    {
        yield return new WaitForSeconds(bulletLifetime);
        Destroy(gameObject);

    }



}
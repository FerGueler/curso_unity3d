﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet2Moves : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 playerDir2;
    GameObject theTank2;
    //public GameObject player;
    public float bulletSpeed;
    public float bulletLifetime;
    // Start is called before the first frame update
    void Start()
    {
        theTank2 = GameObject.Find("Tank2");
        MoveTank2 tankScript = theTank2.GetComponent<MoveTank2>();
        playerDir2 = tankScript.player2Dir;
        // Calling BulletDies funcion
        StartCoroutine(BulletDies());


    }

    // Update is called once per frame
    void Update()

    {
        // ahora mismo al disparo del Tank2 se le aplica la rotacion del tank1, si
        // su rotacion es 0 (mira hacia arriba) dispara bien
        // Tank1 dispara con rotacion doble

        transform.Translate(playerDir2.normalized * bulletSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall Vertical"))
        {
            //Destroy(gameObject);
            playerDir2 = new Vector3(-playerDir2.x, playerDir2.y, playerDir2.z);




        }
        if (collision.gameObject.CompareTag("Wall Horizontal"))
        {
            playerDir2 = new Vector3(playerDir2.x, -playerDir2.y, playerDir2.z);


        }
    }
    IEnumerator BulletDies()
    {
        yield return new WaitForSeconds(bulletLifetime);
        Destroy(gameObject);

    }
}


using System;

public class Funciones 
public class EstructurasDeControl 
{
	public static void Main() 
	{
		/* 
		EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();
		*/
		EjempoIfConsecutivos();
	}
	
	static void EjemploIfSimple()
	{
		// Condicional simple:	if (booleano) instruccion;
		if (true) Console.WriteLine("Pues sí");
		if (false) Console.WriteLine("Pues va a ser que no");
		
		// Recibe variable
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues también sí");
		// O recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 == 5");
		if (4 > 7) Console.WriteLine("Pues esto tampoco se muestra");
	}
	static void EjemploIfComplicado()
	{				
		// IF complicado: 
		// if (bool) instruccionVerdad; else instruccionFalso;
		if (4 >= 7) Console.WriteLine("4 >= 7"); 
		else Console.WriteLine("4 < 7");
		// Podemos separar en varias lineas
		if ("Hola" != "hola") Console.WriteLine("Son dist"); 
		else Console.WriteLine("Son =");	
	}
	static void EjercicioIfSumas() 
	{
		
		// Ejercicio:
		string numA = "20", numB = "30", numC = "40";
		int resultado = 60;
		// Suma las 3 combinaciones (A + B,  B+C y A+C) y que el programa diga cual
		// es igual a resultado.
		// Consola debe mostrar:
		// 1ro mostrar los valores: A = 20, B = 30, C = 40, resultado = 50
		// A + B es igual a resultado
		// A + C es distinto de resultado
		// B + C es distinto de resultado
		int intA, intB, intC;
		intA = Int32.Parse(numA);
		intB = Int32.Parse(numB);
		intC = Int32.Parse(numC);
		
		if (intA + intB  == resultado) 
			Console.WriteLine(" A + B es igual a resultado");
		else
			Console.WriteLine(" A + B es distinto de resultado");
		
		if (intA + intC  == resultado) 
			Console.WriteLine(" A + C es igual a resultado");
		else
			Console.WriteLine(" A + C es distinto de resultado");
		
		if (intB + intC  == resultado) 
			Console.WriteLine(" B + C es igual a resultado");
		else
			Console.WriteLine(" B + C es distinto de resultado");		
		
	}

	static void EjempoIfConsecutivos() {
		Console.WriteLine("Introduzca una opcion: ");
		Console.WriteLine(" 1 - Opcion primera ");
		Console.WriteLine(" 2 - Opcion segunda");
		Console.WriteLine(" 3 - Opcion tercera ");
		Console.WriteLine("(?) - Cualquier otra opcion.");
		
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
		
		Console.WriteLine("\n >> " + caracter);
		// Si el caracter es un 1 del teclao normal o bien
		// si el caracter es un 1 del teclao numérico
		if (caracter == "NumPad1"  || caracter == "D1" ) 
			Console.WriteLine(" Has elegido la primera ");
		else if (caracter == "NumPad2" || caracter == "D2")
			Console.WriteLine(" Has elegido la segunda ");
		else if (caracter == "NumPad3" || caracter == "D3")
			Console.WriteLine(" Has elegido la tercera ");
		else if (caracter == "NumPad4" || caracter == "D4")
			Console.WriteLine(" Has elegido la cuarta ");
		else 
			Console.WriteLine(" OPCION NO CONTEMPLADA ");
	}
}






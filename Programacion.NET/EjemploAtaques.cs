using System;

public class EjemploAtaques 
{	
	public static void Main() 
	{
		float[] arrayAtaque = {3.2f,1.7f,2.4f,5.0f,7.1f,4.8f};
		bool[] arrayEnemCerca = {false,true,true,false,true,true};
		
		//float result1 =SumaDeAtaques(arrayAtaque, arrayEnemCerca);
		//string[] result2 = EnemigosLejos(arrayAtaque, arrayEnemCerca); //ej2
		//float result3 =	SumaEnemigosMayores(arrayAtaque,3.0f); //ej3
		//float result4 = MaxEnemigo(arrayAtaque);//ej4
		//float result5 = MediaAtaques(arrayAtaque);//ej5
		//float result6 = MinAtaqueCerca(arrayAtaque, arrayEnemCerca); //ej6
		//float[] result7 = DobleEnemigos(arrayAtaque); //ej7
	}
	public static float SumaDeAtaques(float[] arrayFloat,bool[] arrayBool)
	{
		int n;
		float suma = 0;
		if (arrayFloat.Length == arrayBool.Length)
			{
				n = arrayFloat.Length;
				for (int i=0; i < n; i++ )
				{
					if(arrayBool[i] /*= true*/)
						{
							suma = suma + arrayFloat[i];
						}
			
				}
				Console.WriteLine(suma);
				return suma;
			}
		else
			{Console.WriteLine("Error: La longitud de los arrays no coincide");
			return suma;}
		
		
	}
	// Ejercicio 2: Generar un array con textos con la info de los NO cercanos:
	// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}
	
	public static string[] EnemigosLejos(float[] arrayFloat,bool[] arrayBool)
	{
		int n=0;
		int m=0;
		int z=0;
		if (arrayFloat.Length == arrayBool.Length)
		{
			n = arrayFloat.Length;
			
			for (int i=0; i < n; i++ )
				{
					if(!arrayBool[i] /*= true*/)
						{
							m++;
						}
				}
				
		int[] indexEnemLejanos = new int[m];
		float[] floatEnemLejanos = new float[m];
		string[] strEnemLejanos = new string[m];
		
		for (int i=0; i < n; i++ )
				{
					if(!arrayBool[i] /*= true*/)
						{
							indexEnemLejanos[z] = i;
							z++;
						}
				}
			
			for(int j=0; j < m; j++)
			{
				floatEnemLejanos[j] = arrayFloat[indexEnemLejanos[j]];
				strEnemLejanos[j]= $"Enemigo {indexEnemLejanos[j]}: {floatEnemLejanos[j]} ptos";
				
			}
			for (int l =0; l<m; l++)
			{
				Console.WriteLine(strEnemLejanos[l]);
				
			}
			return strEnemLejanos;
		}
		else
		{
			Console.WriteLine("Error: La longitud de los arrays no coincide");
			string[] error = new string[] {"Error: La longitud de los arrays no coincide"};
			return error;
			}
			
	}
		
		// Ejercicio 3: Crear una función que calcule el ataque total de los que tengan ataque > 3
	    // 				este tope debe pasarse por parámetro
		
	public static float SumaEnemigosMayores(float[] arrayFloat,float min)
	{
			int n = arrayFloat.Length;
			float suma = 0;
		
				for (int i=0; i < n; i++ )
				{
					if(arrayFloat[i] > min)
						{
							suma = suma + arrayFloat[i];
						}
			
				}
			Console.WriteLine(suma);
			return suma;
				
	
		
	}
	
		// Ejercicio 4: Otra que devuelva el ataque máximo del array
	public static float MaxEnemigo( float[] arrayFloat)
	{
			int n = arrayFloat.Length;
			float currentMax = arrayFloat[0];
			for (int i=1; i <n; i++)
			{
				if (arrayFloat[i]> currentMax)
				{
					currentMax= arrayFloat[i];
				}
			}
			Console.WriteLine(currentMax);
			return currentMax;
			
	}
		// Ejercicio 5: Crear una función que calcule la media de todos los ataques
	public static float MediaAtaques(float[] arrayFloat)
	{
		int n = arrayFloat.Length;
		float suma= 0;
		float media = 0;
				for (int i=0; i < n; i++ )
				{
						
					suma = suma + arrayFloat[i];
						
				}
		media = suma/n;
		Console.WriteLine(media);
		return media;
		
	}
			
		// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
	public static float MinAtaqueCerca(float[] arrayFloat,bool[] arrayBool)
	{
		/*
		int n = arrayFloat.Length;
		float currentMinCerca = 0;
		int indicePrimerCercano = 0;
		{for (int j=0; j<n; j++}
		if (arrayBool[j])
		{	 indicePrimerCercano = j;}
		else
		{
		 if arrayFloat[j-1]!=0;
		}
			for (int i=1; i <n; i++)
			{
				if (arrayFloat[i]> currentMax)
				{
					currentMax= arrayFloat[i];
				}
			}
			Console.WriteLine(currentMax);
			return currentMax;
		*/
		int n=0;
		int m=0;
		if (arrayFloat.Length == arrayBool.Length)
		{
			n = arrayFloat.Length;
			
			for (int i=0; i < n; i++ )
				{
					if(arrayBool[i] /*= true*/)
						{
							m++;
						}
				}
				
		
		float[] floatEnemCercanos = new float[m];
		

			for(int j=0; j < m; j++)
			{
				floatEnemCercanos[j] = arrayFloat[j];
			}
			float currentMin = floatEnemCercanos[0];
			
			for (int z=1; z <m; z++)
			{
				if (floatEnemCercanos[z]< currentMin)
				{
					currentMin= floatEnemCercanos[z];
				}
			}
			Console.WriteLine(currentMin);
			return currentMin;
		}
			else
			{
			Console.WriteLine("Error: La longitud de los arrays no coincide");
			return 0.0f;
			}
	}
	
	
		// Ejercicio 7: Crear una función que devuelva un array que contenga el doble de los ataques:  { 1.6f, 0.85f, ....}
		
		public static float[] DobleEnemigos(float[] arrayFloat)
		{
			int n = arrayFloat.Length;
			float[] resultadoDoble = new float[n];
			for(int i=0;i<n;i++)
			{
				resultadoDoble[i]= 2*arrayFloat[i];
				Console.WriteLine(resultadoDoble[i]);
			}
			
			return resultadoDoble;
		}
		

}

	
using System;

public class ConversionesDatos {
	public static void Main() {
		// Convertir número en texto y float en double
		// Conversiones implícitas:
		int edad = 40;
		string resultado;
		resultado = "" + edad;
		resultado = resultado + " era un número y ahora es texto";		
		Console.WriteLine( resultado );	
		double numDecimal = 6.4545f;
		resultado = resultado + ", el numDecimal = " + numDecimal;
		Console.WriteLine( resultado );	
		
		// Conversiones explícitas, se pone entre paréntesis el tipo:
		float otroDecimal = (float) 1.23456789123;
		resultado = resultado + ", otroDecimal = " + otroDecimal;
		Console.WriteLine( resultado );	
		
		// Conversiones complejas: de texto a número
		int unEntero = Int32.Parse("3434");
		resultado = resultado + ", unEntero = " + unEntero;
		Console.WriteLine( resultado );
		
		string numA = "15", numB = "7";
		// Haz que el programa calcule la suma y muestre el resultado
		int numAInt= Int32.Parse(numA);
		int numBInt= Int32.Parse(numB);
		int suma = numAInt + numBInt;
		Console.WriteLine("numA= " + numA );
		Console.WriteLine("numB= " + numB );
		Console.WriteLine("Suma numA y numB= " + suma );
		
	}
}
	


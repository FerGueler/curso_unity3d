﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float playerSpeed;
    private bool isOnLand;
    private bool hasDoubleJumped;
    public Rigidbody playerRB;
    public float jumpStrength = 1;
    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        isOnLand = true;
        hasDoubleJumped = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow)) //&& isOnLand)
        {

            Vector3 vectorMov = transform.right * playerSpeed * Time.deltaTime;

            GetComponent<Transform>().Translate(vectorMov);


        }
        if (Input.GetKey(KeyCode.LeftArrow)) //&& isOnLand)
        {

            Vector3 vectorMov = -transform.right * playerSpeed * Time.deltaTime;
            GetComponent<Transform>().Translate(vectorMov);
            //playerRB.velocity = new Vector3(-10, 0 , 0);



        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && isOnLand)
        {
            
                playerRB.AddForce(jumpStrength * transform.up, ForceMode.Impulse);
                isOnLand = false;
            
            /*else if (!hasDoubleJumped)
            {
                Vector3 vectorMov = transform.up * playerSpeed * Time.deltaTime;
                GetComponent<Transform>().Translate(vectorMov);
                isOnLand = false;
                hasDoubleJumped = true;
            }
            */
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
         isOnLand=true;
         hasDoubleJumped=false; 
        }
    }
    

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoRed : MonoBehaviour
{
    public float velocidadY; // Por defecto, cero
    public float velocidadX; // Por defecto, cero
    GameObject jugador;
    private float redWidth = 1.2f;
    private float redHeight = 1.2f;
    private float caballitoWidth = 1.2f;
    private float caballitoHeight = 1.2f;
    private float limiteX = 13.0f;
    GameObject spawner;
    public bool lataMurio = false;
    private bool redCogida;
    private float tiempoRed;



    // Start se llama la primera vez, primer frame
    void Start()
    {
        float posInicioX = Random.Range(-limiteX, limiteX);
        this.transform.position = new Vector3(posInicioX, transform.position.y, transform.position.z);
        // Buscamos un GameObject por su nombre, sólo hacer en Start()
        jugador = GameObject.Find("JugadorCaballito");
        spawner = GameObject.Find("SpawnManager");
        redCogida = false;
        tiempoRed = 7.0f;



    }

    // Update is called once per frame
    void Update()
    {

        Vector3 movAbajo = velocidadY * new Vector3(0, 1, 0) * Time.deltaTime;

        


        Vector3 movDerecha = velocidadX * new Vector3(1, 0, 0) * Time.deltaTime;

        if (!redCogida)
        {
            this.GetComponent<Transform>().position = this.GetComponent<Transform>().position
                + movAbajo + movDerecha;
        }


        if (this.GetComponent<Transform>().position.y < -2 || this.GetComponent<Transform>().position.y >10)
        {
            velocidadY = -velocidadY;
        }
        if (this.GetComponent<Transform>().position.x < -14 || this.GetComponent<Transform>().position.x > 14)
        {
            velocidadX = -velocidadX;
        }


        if (((transform.position.x + redWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x + redWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y + redHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y + redHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2) ||
            (transform.position.x - redWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x - redWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y + redHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y + redHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2) ||
            (transform.position.x - redWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x - redWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y - redHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y - redHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2) ||
            (transform.position.x + redWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x + redWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y - redHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y - redHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2)) &&
            !redCogida)
        {
            
            redCogida = true;
            transform.position = jugador.transform.position;

    

        }
        if (redCogida)
        {
            Personaje jugadorScript = jugador.GetComponent<Personaje>();
            transform.position = jugador.transform.position;
            jugadorScript.speed = 12;
            tiempoRed -= Time.deltaTime;
            if (tiempoRed <= 0)
            {
                jugadorScript.speed = 30;
                Destroy(gameObject); ManageSpawns spawnerScript = spawner.GetComponent<ManageSpawns>();
                spawnerScript.crearRed = true;
            }
           



        }

    }
}


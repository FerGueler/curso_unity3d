﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageSpawns : MonoBehaviour
{
    public GameObject prefabBotella;
    public GameObject prefabLata; 
    public GameObject prefabRed;
    public GameObject[] prefabEnemigos;
    public AparicionEnemigo[] aparicionesEnem;
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public bool crearLata = false; 
    public bool crearBotella = false;
    public bool crearEnemigo = false;
    public bool crearRed = false;

    private float timeIni;
    private int i;
    private bool gameOver = false;
    private int enemActual;

    // Start is called before the first frame update
    void Start()
    {
        timeIni = Time.time;
        enemActual = 0;
        i = Random.Range(0, 3);
        crearEnemigo = true;
        crearRed = true;
    }

    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "Vidas: " + vidas;
        textoPuntos.GetComponent<Text>().text = "Puntos: " + puntos;
        // para saber si tiene q aparecer un enemigo tenemos que saber cuanto tiempo ha pasado 
        //desde el inicio del nivel hasta el momento actual 
        // y si es superior al tiempo configurado en la aparicion del enemigo
        float tiempoActual = Time.time - timeIni;
        


            if (tiempoActual > aparicionesEnem[enemActual].tiempoInicio)
            {
                if (!aparicionesEnem[enemActual].yaHaAparecido)
                {
                    Instantiate(prefabEnemigos[aparicionesEnem[enemActual].indiceEnem], new Vector3(aparicionesEnem[enemActual].posInicioX, 11), Quaternion.identity);
                    aparicionesEnem[enemActual].yaHaAparecido = true;
                    enemActual++;
                }

            }
        
        if (vidas < 1)
        { gameOver = true; }
        /*
        if (crearEnemigo && !gameOver)
        {
            i = Random.Range(0, 3);
            Instantiate(prefabEnemigos[i]);
            crearEnemigo = false;
        }
        */
        if (crearRed && !gameOver)
        { 
            Instantiate(prefabRed);
            crearRed = false;
        }
        
    }
}

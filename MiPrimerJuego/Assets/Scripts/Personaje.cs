﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public float speed = 50.0f;
    private float limiteX = 13.0f;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            Vector3 vectorMov = Vector3.left * speed * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
            

            

        }
        if (this.GetComponent<Transform>().position.x < -limiteX)
        {
            this.GetComponent<Transform>().position = new Vector3(-limiteX, 0, 0);
            Debug.Log("el caballito se chocó en la izqueirda");

        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.rotation =  Quaternion.Euler(0, 180, 0);
            Vector3 vectorMov = Vector3.left * speed * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
            
            
        }
        if (this.GetComponent<Transform>().position.x > limiteX)
        {
            this.GetComponent<Transform>().position = new Vector3(limiteX, 0, 0);
            Debug.Log("el caballito se chocó en la derecha");
        }
        


    }
}

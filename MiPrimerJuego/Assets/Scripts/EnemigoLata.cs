﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour
{
    public float velocidad; // Por defecto, cero
    GameObject jugador;
    private float lataWidth = 1.6f;
    private float lataHeight = 1.6f;
    private float caballitoWidth = 3.2f;
    private float caballitoHeight = 3.2f;
    private float limiteX = 13.0f;
    GameObject spawner;
    public bool lataMurio = false;
    
   

    // Start se llama la primera vez, primer frame
    void Start()
    {
        //float posInicioX = Random.Range(-limiteX, limiteX);
        //this.transform.position = new Vector3(posInicioX, transform.position.y, transform.position.z);
        // Buscamos un GameObject por su nombre, sólo hacer en Start()
        jugador = GameObject.Find("JugadorCaballito");
        spawner = GameObject.Find("SpawnManager");
        
        


    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position
            + movAbajo;

        if (this.GetComponent<Transform>().position.y < -2)
        {
                
            //this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -2, transform.position.z);
            GameObject.Find("SpawnManager").GetComponent<ManageSpawns>().vidas--;
            Destroy(gameObject);
            ManageSpawns spawnerScript = spawner.GetComponent<ManageSpawns>();
            spawnerScript.crearEnemigo = true;

        }
        /*
        if (this.transform.position.x >= jugador.transform.position.x - caballitoWidth/2 &&
            this.transform.position.x <= jugador.transform.position.x + caballitoWidth /2 &&
            this.transform.position.y >= jugador.transform.position.y - caballitoHeight /2 && 
            this.transform.position.y <= jugador.transform.position.y + caballitoHeight /2)
        {
            Destroy(gameObject);
        }
        */

        if ((transform.position.x + lataWidth/2>= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x + lataWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y + lataHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y + lataHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2)|| 
            (transform.position.x - lataWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x - lataWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y + lataHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y + lataHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2)||
            (transform.position.x - lataWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x - lataWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y - lataHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y - lataHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2)||
            (transform.position.x + lataWidth / 2 >= jugador.transform.position.x - caballitoWidth / 2 &&
            transform.position.x + lataWidth / 2 <= jugador.transform.position.x + caballitoWidth / 2 &&
            transform.position.y - lataHeight / 2 >= jugador.transform.position.y - caballitoHeight / 2 &&
            transform.position.y - lataHeight / 2 <= jugador.transform.position.y + caballitoHeight / 2))
        {
            Destroy(gameObject);
            ManageSpawns spawnerScript = spawner.GetComponent<ManageSpawns>();
            spawner.GetComponent<ManageSpawns>().puntos++;
        
            spawnerScript.crearEnemigo = true;

        }
    }
}


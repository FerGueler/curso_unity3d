﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovesRight : MonoBehaviour
{
    public float enemySpeed = 20;
    private float rightBound = 18;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Moves right
        transform.Translate(Vector3.right * enemySpeed* Time.deltaTime);
        //Dies when off-screen
        if (transform.position.x > rightBound)
        {
            Destroy(gameObject);
        }

        
    }
    private void OnMouseDown()
    {
        Destroy(gameObject);
    }
}

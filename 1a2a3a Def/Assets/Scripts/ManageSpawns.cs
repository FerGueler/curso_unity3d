﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSpawns : MonoBehaviour
{
    public float spawnerSpeed;
    public float yBound =3.6f;
    public GameObject enemy1;
    private float spawnRate = 1.65f;
    private float firstTime = 1;
    
    // Start is called before the first frame update
    void Start()
    {

        InvokeRepeating("spawnEnemy1", spawnRate, firstTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    //Moves up and down
    transform.Translate(Vector3.up * spawnerSpeed * Time.deltaTime);

        if (transform.position.y > yBound || transform.position.y < -yBound)
        {
            spawnerSpeed = -spawnerSpeed;
        }

        //Spawn Enemies

    }

    public void spawnEnemy1()
    { 
        Instantiate(enemy1, transform.position, transform.rotation);
    }
}

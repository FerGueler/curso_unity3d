﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMio : MonoBehaviour
{
    public float playerSpeed;
    private bool isOnLand;
    private bool hasDoubleJumped;
    public Rigidbody2D playerRB;
    public float jumpStrength = 20;
    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
        isOnLand = true;
        hasDoubleJumped = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && isOnLand)
        {

            Vector3 vectorMov = transform.right * playerSpeed * Time.deltaTime;

            GetComponent<Transform>().Translate(vectorMov);


        }
        if (Input.GetKey(KeyCode.LeftArrow) && isOnLand)
        {

            Vector3 vectorMov = -transform.right * playerSpeed * Time.deltaTime;
            GetComponent<Transform>().Translate(vectorMov);
            //playerRB.velocity = new Vector3(-10, 0 , 0);



        }
        if (Input.GetKey(KeyCode.Space) && isOnLand)
        {

            playerRB.AddForce(jumpStrength * transform.up, ForceMode2D.Impulse);
            isOnLand = false;

            /*else if (!hasDoubleJumped)
            {
                Vector3 vectorMov = transform.up * playerSpeed * Time.deltaTime;
                GetComponent<Transform>().Translate(vectorMov);
                isOnLand = false;
                hasDoubleJumped = true;
            }
            */
        }
        /*
        if (Input.GetKey(KeyCode.UpArrow) && isOnLand)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {playerRB.AddForce(jumpStrength * transform.up, ForceMode2D.Impulse);
            isOnLand = false; }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                playerRB.AddForce(jumpStrength * transform.up, ForceMode2D.Impulse);
                isOnLand = false;
            }

        }
        */
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnLand = true;
            hasDoubleJumped = false;
        }
    }
}

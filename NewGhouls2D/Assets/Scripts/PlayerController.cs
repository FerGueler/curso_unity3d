﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    private bool hasDoubleJumped = false;
    private bool isOnGround = true;
    private float playerSpeed = 5.0f;
    //private float maxFallSpeed = -7.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
       
    }


    void FixedUpdate()
    {
        //bug al caer en lugar de hacer el primer salto no se pierde IsOnGround y se pueden hacer 2 saltos mas
        if (Input.GetKeyDown(KeyCode.A) && Input.GetKey(KeyCode.RightArrow))
        {
            if (isOnGround)
            {
                rb.velocity = new Vector2(5, 7);

                Debug.Log("jump");
                isOnGround = false;
            }
            else if (!hasDoubleJumped)
            {
                rb.velocity = new Vector2(5, 7);

                Debug.Log("jump");
                isOnGround = false;
                hasDoubleJumped = true;
            }
        }
    
        if (Input.GetKeyDown(KeyCode.A) && Input.GetKey(KeyCode.LeftArrow))
        {
            if (isOnGround)
            {
                rb.velocity = new Vector2(-5, 7);

                Debug.Log("jump");
                isOnGround = false;
            }
            else if (!hasDoubleJumped)
            {
                rb.velocity = new Vector2(-5, 7);

                Debug.Log("jump");
                isOnGround = false;
                hasDoubleJumped = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.A) && !Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            if (isOnGround)
            {
                rb.velocity = new Vector2(0, 7);

                Debug.Log("jump");
                isOnGround = false;
            }
            else if (!hasDoubleJumped)
            {
                rb.velocity = new Vector2(0, 7);

                Debug.Log("jump");
                isOnGround = false; 
                hasDoubleJumped = true;
            }
        }
        /*
        if (Input.GetKey(KeyCode.Space))
        {
            rb.velocity = new Vector2(0, 10);
            isMoving = true;
            Debug.Log("jump");
            isOnGround = false;
        }
        */


        if (Input.GetKey(KeyCode.RightArrow) && isOnGround)
        {

            Vector3 vectorMov = transform.right * playerSpeed * Time.deltaTime;

            GetComponent<Transform>().Translate(vectorMov);
            
        }
        if (Input.GetKey(KeyCode.LeftArrow) && isOnGround)
        {
            
            Vector3 vectorMov = -transform.right * playerSpeed * Time.deltaTime;

            GetComponent<Transform>().Translate(vectorMov);
            
        }
       
        // este if es para limitar la velocidad maxima de caida, hace que la caida deje de ser una parabola
        // y pasa a ser una recta diagonal cuando alcanza la velocidad maxima. No estoy seguro de si es bueno
        // limitar la velocidad maxima.
        /* 
        if (rb.velocity.y <= maxFallSpeed)
        {
            rb.velocity = new Vector2(rb.velocity.x, maxFallSpeed);
        }
        */
        Debug.Log(rb.velocity);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            rb.velocity = new Vector2(0, 0);
            isOnGround = true;
            hasDoubleJumped = false;
        }
    }

    
}

